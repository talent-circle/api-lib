"use strict";

const momentTZ = require('moment-timezone');
const Api = require('./Api');

const insert = (entity, slug) => Api.post(`${slug}`, entity);
const update = (entity, slug) => Api.put(`${slug}/${entity.id}`, entity);

class BaseEntity {
  constructor() {
    this.slug = null;
  }

  static cleanDates(entity) {
    const newEntity = Object.assign({}, entity);

    if (newEntity.created_at) {
      newEntity.created_at = momentTZ(newEntity.created_at).format('YYYY-MM-DD HH:mm:ss');
    }

    if (newEntity.updated_at) {
      newEntity.updated_at = momentTZ(newEntity.updated_at).format('YYYY-MM-DD HH:mm:ss');
    }

    return newEntity;
  }

  find(uncleanParams) {
    const params = uncleanParams;

    if (params._q === '') {
      params._q = null;
    }

    return Api.get(`${this.slug}`, { params }).then((response) => {
      const newResponse = Object.assign({}, response);
      newResponse.data = [];

      response.data.forEach((result) => {
        newResponse.data.push(BaseEntity.cleanDates(result));
      });

      return newResponse;
    });
  }

  count(uncleanParams) {
    const params = uncleanParams;

    if (params._q === '') {
      params._q = null;
    }

    return Api.get(`${this.slug}/count`, { params });
  }

  get(id) {
    return Api.get(`${this.slug}/${id}`).then((response) => {
      const newResponse = Object.assign({}, response);
      newResponse.data = BaseEntity.cleanDates(newResponse.data);

      return newResponse;
    });
  }

  save(entity) {
    return (entity.id ? update(entity, this.slug) : insert(entity, this.slug));
  }

  delete(entity) { return Api.delete(`${this.slug}/${entity.id}`); }
}

module.exports = BaseEntity;
