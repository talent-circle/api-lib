"use strict";

const BaseEntity = require('./BaseEntity');

class UsersApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'users';
  }
}

module.exports = new UsersApi();
