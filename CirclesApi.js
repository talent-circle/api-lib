"use strict";

const BaseEntity = require('./BaseEntity');

class CirclesApi extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'circles';
  }
}

module.exports = new CirclesApi();
