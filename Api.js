"use strict";

const axios = require('axios');

const Api = axios.create({
  baseURL: process.env.TALENTCIRCLE_API_URL || 'https://api.talentcircle.co',
  timeout: 2000,
});

if (typeof localStorage !== 'undefined' && localStorage && localStorage.userJwt) {
  Api.defaults.headers.common.Authorization = `Bearer ${localStorage.userJwt}`;
}

module.exports = Api;
